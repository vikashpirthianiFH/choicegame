package com.example.admin.myjsonreadexample;

import java.util.ArrayList;

/**
 * Created by Admin on 11.07.2017.
 */

public class Answers {

    private String content ;
    private String userOptionLeft;
    private String userOptionRight;
    private  String  ans;
    private int id ;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUserOptionLeft() {
        return userOptionLeft;
    }

    public void setUserOptionLeft(String userOptionLeft) {
        this.userOptionLeft = userOptionLeft;
    }

    public String getUserOptionRight() {
        return userOptionRight;
    }

    public void setUserOptionRight(String userOptionRight) {
        this.userOptionRight = userOptionRight;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public String getAns() {
        return ans;
    }

    public void setAns(String ans) {
        this.ans = ans;
    }


}

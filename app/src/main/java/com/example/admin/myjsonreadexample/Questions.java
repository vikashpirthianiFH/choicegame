package com.example.admin.myjsonreadexample;

import android.content.Context;
import android.content.res.AssetManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;

/**
 * Created by Admin on 11.07.2017.
 */

public class Questions implements Serializable {


    private String[] question;
    private String[] answers;
    private static String dataString = "";
    private static JSONArray questionArray;
    private String name = "";



    private long id = 0;

    public Questions(String[] question, String name ,String []answers , long id) {

        this.id = id;
        this.question = question;
        this.name = name;
        this.answers= answers;
    }

    public Questions() {
        question = new String[1];
        name = "";
        id = 0;
        answers = new String[1];
    }

    public Questions(Questions aQuestion) {
        this.question = aQuestion.question;
        this.answers = aQuestion.answers;
    }

    public Questions(JSONObject json) {
        try {


             setId(json.getLong("Id"));
            JSONArray questionJsonArray = json.getJSONArray("questions");
            JSONArray answerJsonArray = json.getJSONArray("children");

            //Json Object to get the Question

            String[] questionString = new String[questionJsonArray.length()];
            for (int j = 0; j < questionJsonArray.length(); j++) {
                questionString[j] = questionJsonArray.getJSONObject(j).get("question").toString();
            }

            //Json Object to get the Answers


            String[] answerString = new String[answerJsonArray.length()];
            for (int j = 0; j < answerJsonArray.length(); j++) {
                answerString[j] = answerJsonArray.getJSONObject(j).get("ans").toString();
            }
            setQuestion(questionString);
            setAnswers(answerString);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String[] getQuestion() {
        return question;
    }

    public void setQuestion(String[] question) {
        this.question = question;
    }
    public String[] getAnswers() {
        return answers;
    }

    public void setAnswers(String[] answers) {
        this.answers = answers;
    }

    public static Questions getQuestions(int index) {
        Questions question = null;
        try {
            question = new Questions(questionArray.getJSONObject(index));
        } catch (JSONException jsonException) {

        }
        return question;

    }

    public static JSONArray getQuestionsJSONArray() {
        questionArray = new JSONArray();
        try {
            JSONObject json = new JSONObject(dataString);
            questionArray = json.getJSONArray("data");
        } catch (JSONException jsonException) {

        }
        return questionArray;
    }

    public static void initializeStations(Context context) {

        try {
            AssetManager assetManager = context.getAssets();
        InputStream in = assetManager.open("data.json");
        InputStreamReader isr = new InputStreamReader(in);
        char[] inputBuffer = new char[100];

        int charRead;
        while ((charRead = isr.read(inputBuffer)) > 0) {
            String readString = String.copyValueOf(inputBuffer, 0, charRead);
            dataString += readString;
        }
    } catch (IOException ioe) {
        ioe.printStackTrace();
    }
    }
}

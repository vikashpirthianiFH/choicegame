package com.example.admin.myjsonreadexample;

import android.content.Context;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.wang.avi.AVLoadingIndicatorView;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {

    TextView UserQuestion1;
    TextView UserQuestion2;
    TextView questionView;
    TextView questionView2;
    TextView questionView3;
    TextView questionView4;
    TextView questionView5;


    String questionString;

    ImageView profilePic_2;
    ImageView profilePic_3;
    ImageView profilePic_4;
    ImageView profilePic_5;


    List<String> contentList;
    List<String> LeftOptionList;
    List<String> RightOptionList;
    int contentLength = 0;
    int j = 0;
    private AVLoadingIndicatorView avi;
    private AVLoadingIndicatorView aviTwo;
    private AVLoadingIndicatorView aviThree;
    private AVLoadingIndicatorView aviFour;
    private AVLoadingIndicatorView aviFive;

    TextView loadingView;
    TextView loadingView2;
    TextView loadingView3;
    TextView loadingView4;
    TextView loadingView5;

    String indicator1;
    String indicator2;
    String indicator3;
    String indicator4;
    String indicator5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        questionView = (TextView) findViewById(R.id.questionID1);
        questionView2 = (TextView) findViewById(R.id.questionID2);
        questionView3 = (TextView) findViewById(R.id.questionID3);
        questionView4 = (TextView) findViewById(R.id.questionID4);
        questionView5 = (TextView) findViewById(R.id.questionID5);


        UserQuestion1 = (TextView) findViewById(R.id.user_one);
        UserQuestion2 = (TextView) findViewById(R.id.user_two);

        profilePic_2 = (ImageView) findViewById(R.id.profile_image2);
        profilePic_3 = (ImageView) findViewById(R.id.profile_image3);
        profilePic_4 = (ImageView) findViewById(R.id.profile_image4);
        profilePic_5 = (ImageView) findViewById(R.id.profile_image5);

        loadingView = ((TextView) findViewById(R.id.loading_text1));
        loadingView2 = ((TextView) findViewById(R.id.loading_text2));
        loadingView3 = ((TextView) findViewById(R.id.loading_text3));
        loadingView4 = ((TextView) findViewById(R.id.loading_text4));
        loadingView5 = ((TextView) findViewById(R.id.loading_text5));
//
        indicator1 = getIntent().getStringExtra("indicator1");
        avi = (AVLoadingIndicatorView) findViewById(R.id.avi1);
        avi.setIndicator(indicator1);
//
        indicator2 = getIntent().getStringExtra("indicator2");
        aviTwo = (AVLoadingIndicatorView) findViewById(R.id.avi2);
        aviTwo.setIndicator(indicator2);

        indicator3 = getIntent().getStringExtra("indicator3");
        aviThree = (AVLoadingIndicatorView) findViewById(R.id.avi3);
        aviThree.setIndicator(indicator3);

        indicator4 = getIntent().getStringExtra("indicator4");
        aviFour = (AVLoadingIndicatorView) findViewById(R.id.avi4);
        aviFour.setIndicator(indicator4);

        indicator5 = getIntent().getStringExtra("indicator5");
        aviFive = (AVLoadingIndicatorView) findViewById(R.id.avi5);
        aviFive.setIndicator(indicator4);

        questionView.setVisibility(View.INVISIBLE);

        timer();


        try {
            InputStream is = getApplicationContext().getAssets().open("content");

            // We guarantee that the available method returns the total
            // size of the asset...  of course, this does mean that a single
            // asset can't be more than 2 gigs.
            int size = is.available();

            // Read the entire asset into a local byte buffer.
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();

            // Convert the buffer into a string.
            String questionString1 = new String(buffer);
            // Finally stick the string into the text view.
            questionString = questionString1;
        } catch (IOException e) {
            // Should never happen!
            throw new RuntimeException(e);
        }

        Pattern RightOptionPattern = Pattern.compile("(?<=@).+?(?=@)");
        Pattern contentPattern = Pattern.compile("(?<=<).+?(?=>)");
        Pattern LeftOptionPattern = Pattern.compile("(?<=#).*?(?=#)");


        Matcher contentMatcher = contentPattern.matcher(questionString);

        contentList = new ArrayList<String>();
        while (contentMatcher.find()) {
            contentList.add(contentMatcher.group());
        }
        contentLength = contentList.size();


        Matcher LeftOptionMatcher = LeftOptionPattern.matcher(questionString);

        LeftOptionList = new ArrayList<String>();
        while (LeftOptionMatcher.find()) {
            LeftOptionList.add(LeftOptionMatcher.group());
        }

        Matcher RightOptionMatcher = RightOptionPattern.matcher(questionString);

        RightOptionList = new ArrayList<String>();
        while (RightOptionMatcher.find()) {
            RightOptionList.add(RightOptionMatcher.group());
        }
        String currentContentOfList = contentList.get(j);
        if (currentContentOfList.indexOf("(") >= 0) {
            questionView.setText("Break String");
        } else {
            questionView.setText(currentContentOfList);


        }

        String firstWordsLeft = LeftOptionList.get(j).substring(0, LeftOptionList.get(j).lastIndexOf(" "));
        String firstWordsRight = RightOptionList.get(j).substring(0, RightOptionList.get(j).lastIndexOf(" "));
        UserQuestion1.setText(firstWordsLeft);
        UserQuestion2.setText(firstWordsRight);


        UserQuestion1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Add your code in here!avi.hide();

                String clean = LeftOptionList.get(j).replaceAll("\\D+", "");
                int i = Integer.parseInt(clean);
                i = i - 1;
                j = i;
                if (i < contentLength) {


                    loadingView.setVisibility(View.VISIBLE);
                    loadingView2.setVisibility(View.INVISIBLE);
                    loadingView3.setVisibility(View.INVISIBLE);
                    loadingView4.setVisibility(View.INVISIBLE);
                    loadingView5.setVisibility(View.INVISIBLE);

                    profilePic_2.setVisibility(View.INVISIBLE);
                    profilePic_3.setVisibility(View.INVISIBLE);
                    profilePic_4.setVisibility(View.INVISIBLE);
                    profilePic_5.setVisibility(View.INVISIBLE);

                    questionView.setVisibility(View.INVISIBLE);
                    questionView2.setVisibility(View.INVISIBLE);
                    questionView3.setVisibility(View.INVISIBLE);
                    questionView4.setVisibility(View.INVISIBLE);
                    questionView5.setVisibility(View.INVISIBLE);

                    String contentAfterCleaning = contentList.get(i);
                    if (contentAfterCleaning.indexOf("(") >= 0) {
                        String[] contentParts = contentAfterCleaning.split("\\(");
                        int length = contentParts.length;
                        Log.e("length ", length + "");

                        switch (length) {
                            case 1:
                                questionView.setText(contentParts[0] + length);
                                questionView2.setVisibility(View.INVISIBLE);
                                questionView3.setVisibility(View.INVISIBLE);
                                questionView4.setVisibility(View.INVISIBLE);
                                questionView5.setVisibility(View.INVISIBLE);

                                profilePic_2.setVisibility(View.INVISIBLE);
                                profilePic_3.setVisibility(View.INVISIBLE);
                                profilePic_4.setVisibility(View.INVISIBLE);
                                profilePic_5.setVisibility(View.INVISIBLE);
                                break;
                            case 2:
                                questionView.setText(contentParts[0]);
                                questionView2.setText(contentParts[1]);
                                questionView3.setVisibility(View.INVISIBLE);
                                questionView4.setVisibility(View.INVISIBLE);
                                questionView5.setVisibility(View.INVISIBLE);
                                profilePic_3.setVisibility(View.INVISIBLE);
                                profilePic_4.setVisibility(View.INVISIBLE);
                                profilePic_5.setVisibility(View.INVISIBLE);
                                break;
                            case 3:
                                questionView.setText(contentParts[0]);
                                questionView2.setText(contentParts[1]);
                                questionView3.setText(contentParts[2]);
                                profilePic_4.setVisibility(View.INVISIBLE);
                                profilePic_5.setVisibility(View.INVISIBLE);
                                break;
                            case 4:
                                questionView.setText(contentParts[0]);
                                questionView2.setText(contentParts[1]);
                                questionView3.setText(contentParts[2]);
                                questionView4.setText(contentParts[3]);
                                profilePic_5.setVisibility(View.GONE);
                                questionView5.setVisibility(View.GONE);
                                break;
                            case 5:
                                questionView.setText(contentParts[0]);
                                questionView2.setText(contentParts[1]);
                                questionView3.setText(contentParts[2]);
                                questionView4.setText(contentParts[3]);
                                questionView5.setText(contentParts[4]);
                                break;

                        }

                    } else {
                        questionView.setText(contentAfterCleaning);
                    }

                     avi.show();


                    String firstWordsLeft = LeftOptionList.get(i).substring(0, LeftOptionList.get(i).lastIndexOf(" "));
                    UserQuestion1.setVisibility(View.INVISIBLE);
                    UserQuestion1.setText(firstWordsLeft);
                    String firstWordsRight = RightOptionList.get(i).substring(0, RightOptionList.get(i).lastIndexOf(" "));
                    UserQuestion2.setVisibility(View.INVISIBLE);
                    UserQuestion2.setText(firstWordsRight);
                } else {
                    Context context = getApplicationContext();
                    CharSequence text = "Thank your for using AI ";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                    j = 0;
                }
                timer();
            }

        });
        UserQuestion2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Add your code in here
                String clean = RightOptionList.get(j).replaceAll("\\D+", "");
                int i = Integer.parseInt(clean);
                i = i - 1;
                j = i;
                if (i < contentLength) {

                    questionView.setVisibility(View.INVISIBLE);
                    questionView2.setVisibility(View.INVISIBLE);
                    questionView3.setVisibility(View.INVISIBLE);
                    questionView4.setVisibility(View.INVISIBLE);
                    questionView5.setVisibility(View.INVISIBLE);

                    questionView.setText(contentList.get(i));
                    avi.show();

                    loadingView.setVisibility(View.VISIBLE);
                    loadingView2.setVisibility(View.INVISIBLE);
                    loadingView3.setVisibility(View.INVISIBLE);
                    loadingView4.setVisibility(View.INVISIBLE);
                    loadingView5.setVisibility(View.INVISIBLE);

                    profilePic_2.setVisibility(View.INVISIBLE);
                    profilePic_3.setVisibility(View.INVISIBLE);
                    profilePic_4.setVisibility(View.INVISIBLE);
                    profilePic_5.setVisibility(View.INVISIBLE);
                    questionView2.setVisibility(View.INVISIBLE);

                    UserQuestion1.setVisibility(View.INVISIBLE);

                    String contentAfterCleaning = contentList.get(i);
                    if (contentAfterCleaning.indexOf("(") >= 0) {
                        questionView.setText("Break String");
                    } else {
                        questionView.setText(contentAfterCleaning);
                    }
                    String firstWordsLeft = LeftOptionList.get(i).substring(0, LeftOptionList.get(i).lastIndexOf(" "));
                    UserQuestion1.setText(firstWordsLeft);
                    UserQuestion1.setText(firstWordsLeft);
                    String firstWordsRight = RightOptionList.get(i).substring(0, RightOptionList.get(i).lastIndexOf(" "));
                    UserQuestion2.setVisibility(View.INVISIBLE);
                    UserQuestion2.setText(firstWordsRight);
                } else {
                    Context context = getApplicationContext();
                    CharSequence text = "Thank your for using AI ";
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();
                    j = 0;
                }

                timer();
            }
        });


    }

    public void timer() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadingView.setVisibility(View.INVISIBLE);
                avi.hide();
                questionView.setVisibility(View.VISIBLE);
                loadingView2.setVisibility(View.INVISIBLE);
            }
        }, 1500);

        final Handler handler2 = new Handler();
        handler2.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadingView2.setVisibility(View.VISIBLE);
                aviTwo.show();
                profilePic_2.setVisibility(View.VISIBLE);


            }
        }, 2000);

        final Handler handler3 = new Handler();
        handler3.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadingView2.setVisibility(View.INVISIBLE);
                aviTwo.hide();
                questionView2.setVisibility(View.VISIBLE);
                aviThree.setVisibility(View.VISIBLE);
                profilePic_3.setVisibility(View.VISIBLE);
                loadingView3.setVisibility(View.VISIBLE);

            }
        }, 3000);
        final Handler handler5 = new Handler();
        handler5.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadingView3.setVisibility(View.INVISIBLE);
                aviThree.hide();
                questionView3.setVisibility(View.VISIBLE);
                aviFour.setVisibility(View.VISIBLE);
                profilePic_4.setVisibility(View.VISIBLE);
                loadingView4.setVisibility(View.VISIBLE);

            }
        }, 4000);
        final Handler handler6 = new Handler();
        handler6.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadingView4.setVisibility(View.INVISIBLE);
                aviFour.hide();
                questionView4.setVisibility(View.VISIBLE);
                aviFive.hide();
                profilePic_5.setVisibility(View.INVISIBLE);
                loadingView5.setVisibility(View.INVISIBLE);

            }
        }, 5000);
        final Handler handler7 = new Handler();
        handler7.postDelayed(new Runnable() {
            @Override
            public void run() {
                loadingView5.setVisibility(View.INVISIBLE);
                aviFive.hide();
                questionView5.setVisibility(View.INVISIBLE);


            }
        }, 6000);
        final Handler handler4 = new Handler();
        handler4.postDelayed(new Runnable() {
            @Override
            public void run() {

                UserQuestion1.setVisibility(View.VISIBLE);
                UserQuestion2.setVisibility(View.VISIBLE);
            }
        }, 7000);
    }
}
